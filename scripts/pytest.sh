#!/usr/bin/env sh
# cd ../src/ && python3 manage.py test --settings=superapi.settings.developments
# pytest-django 를 설치 해야 한다..
# pytest 를 할떄는 pytest.ini 생성 후  새로운 테스트 환경 파일 생성후
# .coveragerc 는 커버리지 돌리지 않을 파일 지정
# DJANGO_SETTING_MODULE 로 지정
# -s 옵션이 없으면 ipdb로 트레이스 할때 에러남
# -x --ipdb 는 첫 실패시 브레이크 포인트를 건다
#cd ../src/ && pytest ./ports/tests/test_models.py
cd ../src/ && pytest ./ports/tests/test_views.py