#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    
    '''
    메타 안에 model = User , 가 아니다 ,를 반드시 지워야함
    '''
    class Meta:
      model = User
      fields = (
          'username',
          'password',
          'email',
          'is_active',
      )