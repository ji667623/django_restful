#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User

import pytest

from mixer.backend.django import mixer

from .base import PortsBaseTest

pytestmark = pytest.mark.django_db

# Create your tests here.
# 모델 테스트
# 브레이크 포인트를 건다 ipdb.set_trace()
class PortsModelTest(PortsBaseTest):

    '''
    doc 
    유저 생성 테스트
    '''
    def test_create_user(self):
        User.objects.create(
            username="hell"
        )

        assert User.objects.count() == 1, "equal"

    '''
    doc
    어드민 유저 mixer로 생성 
    '''
    def test_create_super_user_via_mixer(self):
        super_user = mixer.blend('auth.User', is_staff=True, is_superuser=True)
        assert User.objects.count() == 1, "equal"
        assert super_user.is_superuser is True, "super user"

    '''
    doc
    유저 다량 생성 테스트 
    mock X fake O
    '''
    def test_create_user_via_mixer(self):
        for cnt in range(50):
            mixer.blend('auth.User')
        #import ipdb
        #ipdb.set_trace()
        assert User.objects.count() == 50, "equal"