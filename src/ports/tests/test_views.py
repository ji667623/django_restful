#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework import status

import pytest

import json

from mixer.backend.django import mixer

from .base import PortsBaseTest

'''
pytest django 는 디비에 접근하는 테스트에 보수적
기본적으로 디비 접근을 할려고 하면 실패가 뜬다.
테스트 대상에서 디비에 접근한다면 정확한 명시가 필요하다.

@oytest.mark.django_db
def test_db_create(self):

또는 

pytestmark = pytest.mark.django_db

또는

@pytest.mark.django_db
class TestUser
    pytestmark = pytest.mark.django_db
    
    def test_my_user(self):
      
트랜잭션
@pytest.mark.django_db(transaction=True)
 
테스트 전용 DB
--reuse-db 은 데이터베이스를 재사용하기 위한 실행 옵션이고, --create-db 는 데이터베이스를 강제로 다시 생성하는 실행 옵션이다.
처음 테스트를 실행할 때 --reuse-db 를 사용하면 새로운 테스트 전용 DB가 생성되는데 모든 테스트가 종료 되더라도 테스트 DB는 지워지지 않는다. 그리고 다음 테스트를 실행할 때 동일하게 --reuse-db 를 사용하면 이전 테스트DB를 다시 사용하게 된다.
이 옵션은 적은 테스트를 실행할 때나 DB 테이블이 많은 경우 유용한다.

--reuse-db 옵션을 기본 pytest.ini 옵션으로 지정하고, 테이블 스키마가 변경되었거나 했을때 --create-db 옵션을 사용하자.

--nomigrations 를 사용할 경우 Django migrations 와 모든 모델 클래스 검사를 위한 DB 생성을 수행하지 않는다.
'''
pytestmark = pytest.mark.django_db

# Create your tests here.
# 모델 테스트
class PortsViewTest(PortsBaseTest):

    '''
    doc
    '''
    def create_fake_user(self, count):
        #create 50 fake User
        for cnt in range(count):
            mixer.blend('auth.User', is_active=True)

    '''
    doc
    '''
    def test_create_fake_user_then_request_via_user_viewset(self):
        self.create_fake_user(50)
        url = reverse('user-list')
        response = self.client.get(url, format='json')
        assert response.status_code == status.HTTP_200_OK

    '''
    doc
    '''
    def test_request_via_user_viewset(self):
        #list GET:POST
        #retrive GET
        #patch PUT
        #des DELETE
        #get 200 , post 201
        url = reverse('user-list')
        response = self.client.get(url, format='json')
        assert response.status_code == status.HTTP_200_OK

    '''
    doc
    '''
    def test_request_post_via_user_viewset(self):
        url = reverse('user-list')
        data = {
            'username': 'ji6676',
            'password': '123nq12',
            'email': 'bbb@gmail.com',
            'is_active': True,
        }
        response = self.client.post(url,  data, format='json')
        assert response.status_code == status.HTTP_201_CREATED

        response_data = json.loads(response.content)
        assert User.objects.get(pk=1).username == response_data['username'], 'should be equal'

    '''
    doc
    '''
    def test_retrive_update_and_delete_request_via_user_viewset(self):

        url = reverse('user-list')
        data = {
            'username': 'ji6676',
            'password': '123nq12',
            'email': 'bbb@gmail.com',
            'is_active': True,
        }
        response = self.client.post(url,  data, format='json')
        assert response.status_code == status.HTTP_201_CREATED

        url = reverse('user-detail', args=[1])
        response = self.client.get(url, format='json')
        assert response.status_code == status.HTTP_200_OK

        response_data = json.loads(response.content)
        assert User.objects.get(pk=1).username == response_data['username'], 'should be equal'

        update_username = 'jjjjsss'
        url = reverse('user-detail', args=[1])
        data = {
            'username': update_username
        }
        response = self.client.patch(url,  json.dumps(data), content_type="application/json")
        assert response.status_code == status.HTTP_200_OK

        response_data = json.loads(response.content)
        assert update_username == response_data['username'], 'should be equal'

        assert User.objects.get(pk=1).username == response_data['username'], 'should be equal'



