from django.shortcuts import render

# Create your views here.
from django.contrib.auth.models import User

from rest_framework import viewsets

from ports.serializers import UserSerializer

#serializers 모델과 뷰 사이의 중간 매개체 역활

class UserViewSet(viewsets.ModelViewSet):
    serializer_class =  UserSerializer
    queryset = User.objects.all()